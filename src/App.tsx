import React from 'react';
import {Route, Routes} from "react-router-dom";
import {ROUTES_MAP} from "./app/router/routesMap";
import {Home} from "./pages/Home";
import {UserPage} from "./pages/UserPage";


function App() {
    return (
        <Routes>
            <Route path={ROUTES_MAP.HOME.PATH}>
                <Route index element={<Home/>}/>
                <Route path={ROUTES_MAP.USER_BY_ID.PATH} element={<UserPage/>}/>
            </Route>
        </Routes>
    );
}

export default App;

import {IUserCard} from "../types/user";
import {v4} from "uuid";
import dayjs from "dayjs";

export const USERS_LIST: IUserCard[] = [
    {
        "id": v4(),
        "dateCreation": dayjs().add(1, 'day').toISOString(),
        "avatar": "https://cataas.com/cat/ughE5w0OlXauQ81k",
        "name": "Иван",
        "surname": "Иванов",
        "patronymic": "Иванович",
        "email": "ivan.ivanov@example.com",
        "about": "Люблю программировать и изучать новые технологии."
    },
    {
        "id": v4(),
        "dateCreation": dayjs().toISOString(),
        "avatar": "https://cataas.com/cat/2AjkEyDta2fk44NE",
        "name": "Мария",
        "surname": "Петрова",
        "patronymic": "Дмитриевна",
        "email": "maria.petrova@example.com",
        "about": "Увлекаюсь фотографией и путешествиями."
    },
    {
        "id": v4(),
        "dateCreation": dayjs().subtract(3, 'day').toISOString(),
        "avatar": "https://cataas.com/cat/uFgPv1YY0sBy2Xzc",
        "name": "Алексей",
        "surname": "Смирнов",
        "patronymic": "Владимирович",
        "email": "aleksey.smirnov@example.com",
        "about": "Преподаю математику в университете, люблю горные лыжи."
    },
    {
        "id": v4(),
        "dateCreation": dayjs().subtract(2, 'day').toISOString(),
        "avatar": "https://cataas.com/cat/cKvmJCAvieTUljW7",
        "name": "Елена",
        "surname": "Кузнецова",
        "patronymic": "Сергеевна",
        "email": "elena.kuznetsova@example.com",
        "about": "Интересуюсь историей искусств и архитектурой."
    },
    {
        "id": v4(),
        "dateCreation": dayjs().subtract(1, 'day').toISOString(),
        "avatar": "https://cataas.com/cat/0RU7ZkgzyvWv8UJG",
        "name": "Дмитрий",
        "surname": "Морозов",
        "patronymic": "Николаевич",
        "email": "dmitry.morozov@example.com",
        "about": "Разработчик программного обеспечения, специализирующийся на искусственном интеллекте."
    }
]

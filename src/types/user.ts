export interface IUserCard {
    id: string;
    dateCreation: string;
    avatar?: string;
    name: string;
    surname: string;
    patronymic?: string;
    email: string;
    about?: string;
}

export type TCreateUser = Pick<IUserCard, "avatar" | "name" | "surname" | "patronymic" | "email" | "about">;

export type TUsersMap = Record<string, IUserCard>;

export interface IRemoveUsersData {
    data: TUsersMap;
    count: number;
}



import React, {useState} from "react";
import UsersList from "../../components/UserList/UsersList";
import {CreateUserModal} from "../../components/CreateUserModal";
import {Button} from "@mui/material";
import {IRemoveUsersData, IUserCard} from "../../types/user";
import {useSelector} from "react-redux";
import {DeleteUserModal} from "../../components/DeleteUserModal";
import {selectUsersMap} from "../../redux/cardUser/selectors";

export const Home = () => {
    const usersMap = useSelector(selectUsersMap);
    const [isOpenCreateUserModal, setIsOpenCreateUserModal] = useState(false);
    const [selectedUsersMap, setSelectedUsersMap] = useState<IRemoveUsersData>(
        {data: {}, count: 0}
    );
    const [isSelectMode, setIsSelectMode] = useState<boolean>(false);

    const [isOpenRemoveUsersModal, setIsOpenRemoveUserModal] = useState(false);

    const handleToggleCreateUserModal = () => setIsOpenCreateUserModal(prev => !prev);
    const handleCloseCreateUserModal = () => setIsOpenCreateUserModal(false);
    const handleOpenRemoveUsersModal = () => setIsOpenRemoveUserModal(true);
    const handleCloseRemoveUserModal = () => setIsOpenRemoveUserModal(false);
    const handleActivateSelectMode = () => {
        setIsSelectMode(true);
    }
    const handleStopSelectMode = () => {
        setIsSelectMode(false);
        setSelectedUsersMap({data: {}, count: 0});
    }
    const handleSuccessRemoveUser = () => {
        handleCloseRemoveUserModal();
        handleStopSelectMode();
    }

    const handleSelectAllUsers = () => {
        setSelectedUsersMap({data: usersMap, count: Object.values(usersMap).length});
    }

    const handleToggleSelectUser = (user: IUserCard) => {
        setSelectedUsersMap((map) => {
            const prevMap = {...map};
            const userInMap = prevMap.data[user.id];

            if (userInMap) {
                delete prevMap.data[user.id];
                prevMap.count--;
                return prevMap;
            }

            prevMap.data[user.id] = user;
            prevMap.count++;
            return prevMap;
        })
    }

    return (
        <>
            <div className="w-full flex flex-row justify-center">
                <div className="w-3/4 flex flex-col bg-white p-4">
                    <div className="mb-4 flex gap-x-3">
                        <Button variant="contained" onClick={handleToggleCreateUserModal}>
                            Добавить
                        </Button>
                        {isSelectMode ? (
                            <>
                                <Button variant="contained" onClick={handleStopSelectMode}>
                                    Отменить выбор
                                </Button>
                                <Button variant="contained" onClick={handleSelectAllUsers}>
                                    Выбрать всех
                                </Button>
                            </>
                        ) : (
                            <Button variant="contained" onClick={handleActivateSelectMode}>
                                Выбрать
                            </Button>
                        )}
                        {isSelectMode && selectedUsersMap.count > 0 && (
                            <Button onClick={handleOpenRemoveUsersModal}>
                                Удалить {selectedUsersMap.count} пользователей
                            </Button>
                        )}
                    </div>
                    <UsersList
                        selectedUsers={selectedUsersMap.data}
                        onSelectUser={handleToggleSelectUser}
                        isSelectMode={isSelectMode}
                    />
                </div>
            </div>
            <CreateUserModal
                isOpen={isOpenCreateUserModal}
                onClose={handleCloseCreateUserModal}
            />
            <DeleteUserModal
                onSuccessDelete={handleSuccessRemoveUser}
                isOpen={isOpenRemoveUsersModal}
                onClose={handleCloseRemoveUserModal}
                users={selectedUsersMap}
            />
        </>
    )
}

import React, {useState} from "react";
import {Link, useNavigate, useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {selectUserById} from "../../redux/cardUser/selectors";
import {UserInfo} from "../../components/UserInfo";
import {IRemoveUsersData, IUserCard} from "../../types/user";
import {CreateUserModal} from "../../components/CreateUserModal";
import {DeleteUserModal} from "../../components/DeleteUserModal";
import {generatePath} from "react-router";
import {ROUTES_MAP} from "../../app/router/routesMap";
import {ArrowBack} from "@mui/icons-material";


export const UserPage = () => {
    const {id = ''} = useParams<{ id: string }>();
    const user = useSelector(selectUserById(id));
    const [editInfo, setEditInfo] = useState<IUserCard>();
    const [removedInfo, setRemovedInfo] = useState<IRemoveUsersData>({data: {}, count: 0});
    const navigate = useNavigate();

    if (!user) {
        return <p>
            Пользователь не найден
        </p>
    }

    const handleCloseEditUserModal = () => setEditInfo(undefined);
    const handleOpenEditUserModal = () => setEditInfo(user);
    const handleOpenRemoveModal = () => setRemovedInfo({data: {[user.id]: user}, count: 1});
    const handleCloseRemoveModal = () => setRemovedInfo({data: {}, count: 0});
    const handleSuccessRemoveUser = () => {
        handleCloseRemoveModal();
        navigate(generatePath(ROUTES_MAP.HOME.FULL_PATH), {replace: true});
    }

    return (
        <>
            <div className="w-full flex flex-row justify-center">
                <div className="w-3/4 flex flex-col bg-white p-4 gap-y-3">
                    <Link to={'..'} className="flex flex-row gap-x-2">
                        <ArrowBack/> Вернуться назад
                    </Link>
                    <UserInfo onRemove={handleOpenRemoveModal} user={user} onEdit={handleOpenEditUserModal}/>
                </div>
            </div>
            <CreateUserModal onClose={handleCloseEditUserModal} isOpen={!!editInfo} initialValues={editInfo}/>
            <DeleteUserModal
                isOpen={!!removedInfo.data[user.id]}
                onClose={handleCloseRemoveModal}
                users={removedInfo}
                onSuccessDelete={handleSuccessRemoveUser}
            />
        </>
    );
}

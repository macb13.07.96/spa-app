import {IUserCard} from "../../types/user";
import React from "react";
import {Avatar, Button} from "@mui/material";
import dayjs from "dayjs";

interface IProps {
    user: IUserCard;

    onEdit(): void;

    onRemove(): void;
}

export const UserInfo: React.FC<IProps> = ({user, onEdit, onRemove}) => {
    return (
        <div className="w-full">
            <div className="flex gap-x-5">
                <Avatar alt={`Avatar ${user.surname} ${user.name}`} src={user.avatar} sx={{width: 120, height: 120}}/>
                <div className="flex flex-col gap-y-2">
                    <h1 className="text-2xl font-bold">{user.name} {user.surname}</h1>
                    <div className="flex flex-row gap-x-3">
                        <Button variant="outlined" onClick={onEdit}>
                            Редактировать
                        </Button>
                        <Button variant="outlined" onClick={onRemove}>
                            Удалить
                        </Button>
                    </div>
                    <span className="text-base">
                        На сайте с {dayjs(user.dateCreation).format('DD.MM.YYYY г.')}
                    </span>
                </div>
            </div>
            <div className="border-b-2 mt-5 mb-5"/>
            <div className="flex flex-col gap-y-5">
                <h2 className="text-xl">
                    Данные пользователя
                </h2>
                <div className="flex flex-col gap-y-2">
                    <p className="text-base m-0">Email: {user.email}</p>
                    <p className="text-base m-0">О себе: {user.about}</p>
                </div>
            </div>
        </div>
    )
}

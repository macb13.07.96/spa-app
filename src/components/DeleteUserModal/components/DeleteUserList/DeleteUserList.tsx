import {IRemoveUsersData} from "../../../../types/user";
import React, {useMemo} from "react";
import {useDispatch} from "react-redux";
import {Alert, Button} from "@mui/material";
import {removeUsers} from "../../../../redux/cardUser/slice";

interface IProps {
    users: IRemoveUsersData;

    onDecline(): void;

    onSuccessDelete(): void;
}

export const DeleteUserList: React.FC<IProps> = ({users, onDecline, onSuccessDelete}) => {
    const dispatch = useDispatch();

    const data = useMemo(() => {
        return Object.values(users.data);
    }, [users]);

    const handleRemoveUsers = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        dispatch(removeUsers(data));
        onSuccessDelete();
    }

    return (
        <form className="w-full flex-col flex gap-y-3" onSubmit={handleRemoveUsers}>
            <p className="m-0 text-xl font-bold">Вы уверены что хотите выполнить это действие?</p>

            <Alert severity="warning">
                Данное действие не обратимо
            </Alert>

            <p className="m-0 text-base">Будут удалены следующие пользователи:</p>

            <ol>
                {data.map(({surname, name, patronymic, id}, index) => (
                    <li key={id}>
                        {index + 1}. {surname} {name} {patronymic}
                    </li>
                ))}
            </ol>


            <div className="flex flex-row justify-end gap-x-3 mt-6">
                <Button type="submit" variant="contained">
                    Удалить
                </Button>
                <Button onClick={onDecline}>
                    Отменить
                </Button>
            </div>
        </form>
    )
}

import {Dialog, DialogContent, DialogTitle, IconButton} from "@mui/material";
import {IRemoveUsersData} from "../../types/user";
import React from "react";
import {DeleteUserList} from "./components";
import {Close} from "@mui/icons-material";

interface IProps {
    isOpen: boolean;
    users: IRemoveUsersData;

    onClose(): void;

    onSuccessDelete(): void;
}

export const DeleteUserModal: React.FC<IProps> = ({isOpen, users, onClose, onSuccessDelete}) => {
    return (
        <Dialog sx={{width: "100wh"}} fullWidth open={isOpen} onClose={onClose}>
            <DialogTitle>
                <div className="flex w-full justify-between">
                    <h3 className="m-0 text-xl">
                        {users.count === 1 ? "Удаление пользователя" : `Удаление ${users.count} пользователей`}
                    </h3>
                    <IconButton onClick={onClose}>
                        <Close/>
                    </IconButton>
                </div>
            </DialogTitle>
            <DialogContent>
                <DeleteUserList users={users} onSuccessDelete={onSuccessDelete} onDecline={onClose}/>
            </DialogContent>
        </Dialog>
    )
}

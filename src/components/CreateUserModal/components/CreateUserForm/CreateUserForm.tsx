import {useFormik} from "formik";
import {createUserValidationSchema} from "./validation";
import {IUserCard, TCreateUser} from "../../../../types/user";
import {Avatar, Badge, Button, IconButton, TextField} from "@mui/material";
import React, {useState} from "react";
import {Edit} from "@mui/icons-material";
import {CatImageList} from "../../../CatImageList";
import {useDispatch, useSelector} from "react-redux";
import {selectUserList} from "../../../../redux/cardUser/selectors";
import {baseCatImageURL} from "../../../../app/api";
import {v4} from "uuid";
import dayjs from "dayjs";
import {pasteUser} from "../../../../redux/cardUser/slice";

interface IProps {
    initialValues?: IUserCard,
    userId?: string,

    onDecline(): void;

    onSuccess(): void;
}

const getImageUrl = (id: string) => `${baseCatImageURL}/cat/${id}`;
const defaultImageUrl = getImageUrl("g9nVIulELAJiI8Og");

export const CreateUserForm: React.FC<IProps> = ({initialValues, userId, onDecline, onSuccess}) => {
    const dispatch = useDispatch()
    const users = useSelector(selectUserList);

    const handleSaveUser = (data: IUserCard) => {
        dispatch(pasteUser(data));
        onSuccess();
    }

    const {handleSubmit, values, setFieldValue, errors, touched, isSubmitting} = useFormik<TCreateUser>({
        validationSchema: createUserValidationSchema,
        onSubmit(values, formikHelpers) {
            formikHelpers.setSubmitting(true);
            const hasUserWithExistsEmail = Object.values(users).find((user) => {
                return user.email.toLowerCase() === values.email.toLowerCase() && user.id !== userId;
            })

            if (hasUserWithExistsEmail) {
                formikHelpers.setFieldError('email', 'Пользователь с таким email уже существует!');
                formikHelpers.setSubmitting(false);
                return;
            }

            if (userId && initialValues) {
                handleSaveUser({
                    ...values,
                    dateCreation: initialValues.dateCreation,
                    id: userId,
                });
                formikHelpers.setSubmitting(false);
                return;
            }

            handleSaveUser({
                ...values,
                dateCreation: dayjs().toISOString(),
                id: v4(),
            })
            formikHelpers.setSubmitting(false);
            return;
        },
        initialValues: initialValues ?? {
            avatar: defaultImageUrl,
            surname: "",
            name: "",
            patronymic: undefined,
            email: "",
            about: ""
        }
    });

    const [isOpenImagesList, setIsOpenImagesList] = useState(false);

    const handleToggleImageList = () => setIsOpenImagesList(prev => !prev);

    const handleSelectImage = (id: string) => {
        setFieldValue('avatar', getImageUrl(id));
        setIsOpenImagesList(false);
    }

    return (
        <form
            className="p-4 flex flex-col gap-y-2"
            onSubmit={handleSubmit}
        >
            <div className="flex flex-row gap-x-3 justify-center mb-6">
                <div>
                    <Badge
                        overlap="circular"
                        anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                        badgeContent={
                            <IconButton
                                disabled={isSubmitting}
                                sx={{
                                    opacity: 1,
                                    background: "#fff",
                                    ":hover": {opacity: 1, background: "rgb(230,230,230)",}
                                }}
                                onClick={handleToggleImageList}
                            >
                                <Edit fontSize={"medium"}/>
                            </IconButton>
                        }
                    >
                        <Avatar alt={'Avatar'} src={values?.avatar ?? defaultImageUrl} sx={{width: 120, height: 120}}/>
                    </Badge>
                </div>
                {isOpenImagesList && (
                    <div className="flex flex-col">
                        <CatImageList onSelect={handleSelectImage}/>
                    </div>
                )}
            </div>
            <div className="flex flex-row gap-x-3">
                <TextField
                    value={values.surname}
                    onChange={(event) => setFieldValue('surname', event.target.value)}
                    fullWidth
                    label="Фамилия"
                    required
                    error={!!touched.surname && !!errors.surname}
                    helperText={touched.surname && errors.surname}
                    disabled={isSubmitting}
                />
                <TextField
                    value={values.name}
                    onChange={(event) => setFieldValue('name', event.target.value)}
                    fullWidth
                    label="Имя"
                    required
                    error={!!touched.name && !!errors.name}
                    helperText={touched.name && errors.name}
                    disabled={isSubmitting}
                />
                <TextField
                    value={values.patronymic}
                    onChange={(event) => setFieldValue('patronymic', event.target.value)}
                    label="Отчество"
                    fullWidth
                    error={!!touched.patronymic && !!errors.patronymic}
                    helperText={touched.patronymic && errors.patronymic}
                    disabled={isSubmitting}
                />

            </div>
            <div>
                <TextField
                    value={values.email}
                    onChange={(event) => setFieldValue('email', event.target.value)}
                    fullWidth
                    required
                    label={'Email'}
                    error={!!touched.email && !!errors.email}
                    helperText={touched.email && errors.email}
                    disabled={isSubmitting}
                />
            </div>
            <div>
                <TextField
                    value={values.about}
                    onChange={(event) => setFieldValue('about', event.target.value)}
                    fullWidth
                    label={'О себе'}
                    helperText={touched.about && errors.about}
                    disabled={isSubmitting}
                />
            </div>
            <div className="flex flex-row justify-end gap-x-3 mt-6">
                <Button type="submit" variant="contained" disabled={isSubmitting}>
                    {initialValues && userId ? 'Сохранить' : "Добавить"}
                </Button>
                <Button onClick={onDecline} disabled={isSubmitting}>
                    Отменить
                </Button>
            </div>
        </form>
    )
}

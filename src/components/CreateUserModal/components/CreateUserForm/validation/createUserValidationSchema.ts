import * as yup from 'yup';

export const createUserValidationSchema = yup.object().shape({
    avatar: yup.string().url('Выберите изображение аватара').required('Выбор аватара обязателен'),
    surname: yup.string().required('Фамилия обязательна для заполнения'),
    name: yup.string().required('Имя обязательно для заполнения'),
    patronymic: yup.string().optional(),
    email: yup.string().email('Невалидный email').required('Заполните Email'),
    about: yup.string().optional()
})

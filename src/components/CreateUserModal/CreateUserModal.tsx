import {Dialog, DialogContent, DialogTitle, IconButton} from "@mui/material";
import {CreateUserForm} from "./components";
import {Close} from "@mui/icons-material";
import React from "react";
import {IUserCard} from "../../types/user";

interface IProps {
    isOpen: boolean;
    initialValues?: IUserCard;

    onClose(): void;
}

export const CreateUserModal: React.FC<IProps> = ({onClose, isOpen, initialValues}) => {
    return (
        <Dialog sx={{width: "100wh"}} fullWidth onClose={onClose} open={isOpen}>
            <DialogTitle className="border-b-2">
                <div className="flex flex-row w-full justify-between align-middle">
                    <h3 className="m-0 text-xl">
                        {initialValues ? 'Изменение пользователя' : "Добавление нового пользователя"}
                    </h3>
                    <IconButton onClick={onClose}>
                        <Close/>
                    </IconButton>
                </div>
            </DialogTitle>
            <DialogContent>
                <CreateUserForm
                    initialValues={initialValues}
                    userId={initialValues?.id}
                    onDecline={onClose}
                    onSuccess={onClose}
                />
            </DialogContent>
        </Dialog>
    )
}

import React from "react";
import {UserItem} from "./components";
import {useSelector} from "react-redux";
import {selectUserList} from "../../redux/cardUser/selectors";
import {IUserCard} from "../../types/user";
import {Close} from "@mui/icons-material";

interface IProps {
    selectedUsers?: Record<string, IUserCard>;
    onSelectUser?: (user: IUserCard) => void;
    isSelectMode?: boolean
}

const UsersList: React.FC<IProps> = ({selectedUsers, onSelectUser, isSelectMode}) => {
    const users = useSelector(selectUserList);

    if (!users.length) {
        return <div className='mt-4 flex flex-col gap-y-2 w-full'>
            <Close fontSize={"large"} className="text-center"/>
            <p>Пользователи не найдены</p>
        </div>
    }

    return (
        <div className='mt-4 flex flex-col gap-y-2 w-full'>
            {users.map((user: IUserCard) => (
                <UserItem
                    isSelectMode={isSelectMode}
                    onSelect={onSelectUser}
                    isSelected={!!selectedUsers?.[user.id]}
                    user={user}
                    key={user.id}
                />
            ))}
        </div>
    );
}

export default UsersList;

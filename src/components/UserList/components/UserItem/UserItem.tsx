import React from "react";
import {IUserCard} from "../../../../types/user";
import {Link} from "react-router-dom";
import {generatePath} from "react-router";
import {ROUTES_MAP} from "../../../../app/router/routesMap";
import {Avatar, Checkbox} from "@mui/material";
import dayjs from "dayjs";

interface IProps {
    user: IUserCard;
    isSelected?: boolean;
    onSelect?: (user: IUserCard) => void;
    isSelectMode?: boolean;
}

export const UserItem: React.FC<IProps> = ({user, isSelected, onSelect, isSelectMode}) => {

    const handleSelect = () => {
        onSelect?.(user);
    }

    const linkPath = generatePath(ROUTES_MAP.USER_BY_ID.FULL_PATH, {id: user.id})

    return (
        <div
            className="w-full hover:shadow-md rounded-md p-5 flex justify-start flex-row gap-x-5"
        >
            {isSelectMode && (
                <div className={"h-full flex  items-center"}>
                    <div className="h-fit w-fit">
                        <Checkbox size="large" className="flex-shrink-0" checked={isSelected} onChange={handleSelect}/>
                    </div>
                </div>
            )}
            <Link to={linkPath}>
                <Avatar src={user.avatar} alt="Avatar" sx={{width: 120, height: 120}}/>
            </Link>
            <div className="flex-col flex gap-y-3">
                <Link to={linkPath} className="text-xl underline">
                    {user.surname} {user.name} {user.patronymic}
                </Link>
                <div className='flex flex-col gap-y-1'>
                    <div className="text-base text-gray-600">Email: {user.email}</div>
                    <div className="text-base text-gray-600">На сайте
                        с {dayjs(user.dateCreation).format('DD.MM.YYYY г.')}</div>
                </div>
            </div>
        </div>
    );
};

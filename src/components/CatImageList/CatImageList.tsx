import {ImageList, ImageListItem} from "@mui/material";
import {useGetImageListQuery} from "../../app/api/catApi";
import React, {useState} from "react";

interface IProps {
    onSelect: (id: string) => void;
}

export const CatImageList: React.FC<IProps> = ({onSelect}) => {
    const [page, setPage] = useState(0);
    const {currentData, isFetching} = useGetImageListQuery({page})
    const imageList = currentData ?? [];

    return (
        <ImageList sx={{width: '100%', height: 300, paddingBottom: 10}}>
            {imageList.map((imageData) => (
                <ImageListItem key={imageData._id} sx={{height: 200}} onClick={() => onSelect(imageData._id)}>
                    <img
                        width="50%"
                        height="100px"
                        src={`https://cataas.com/cat/${imageData._id}`}
                        loading="lazy"
                    />
                </ImageListItem>
            ))}
            {isFetching && (
                <p>Загрузка изображений</p>
            )}
        </ImageList>
    )
}

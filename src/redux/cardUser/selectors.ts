import {createSelector} from "@reduxjs/toolkit";
import {RootState} from "../store";
import {IUsersState} from "./types";
import dayjs from "dayjs";

const baseSelector = createSelector(
    (state: RootState) => state.usersSlice,
    (subState) => subState,
)

export const selectUserList = createSelector(
    baseSelector,
    (state) => {
        return Object.values(state.users).sort((a, b) => {
            const dateA = dayjs(a.dateCreation)
            const dateB = dayjs(b.dateCreation);

            if (dateA.isBefore(dateB, 'minute')) {
                return -1;
            }

            if (dateA.isAfter(dateB, 'minute')) {
                return 1;
            }

            return 0;
        })
    },
)

export const selectUserById = (id: string) => createSelector(
    baseSelector,
    (state: IUsersState) => state.users[id],
)

export const selectUsersMap = createSelector(
    baseSelector,
    (state: IUsersState) => state.users,
)

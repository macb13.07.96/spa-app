import {TUsersMap} from "../../types/user";

export interface IUsersState {
    users: TUsersMap;
}

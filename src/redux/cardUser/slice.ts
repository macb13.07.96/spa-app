import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IUserCard} from "../../types/user";
import {IUsersState} from "./types";
import {USERS_LIST} from "../../mock/users";

const generateUsersMap = (users: IUserCard[]): Record<string, IUserCard> => {
    return users.reduce<Record<string, IUserCard>>((acc, value) => {
        acc[value.id] = value;

        return acc;
    }, {})
}

export const initialState: IUsersState = {
    users: generateUsersMap(USERS_LIST),
}

const cardSlice = createSlice({
    name: 'card',
    initialState,
    reducers: {
        pasteUser(state, action: PayloadAction<IUserCard>) {
            state.users[action.payload.id] = action.payload;
        },
        removeUsers(state, action: PayloadAction<IUserCard[]>) {
            const arr = action.payload;

            arr.forEach((user) => {
                delete state.users[user.id];
            })
        }
    },
})
export const {pasteUser, removeUsers} = cardSlice.actions
export default cardSlice.reducer

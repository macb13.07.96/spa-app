import {combineReducers, configureStore} from "@reduxjs/toolkit";
import cardSlice from './cardUser/slice'
import {api} from "../app/api";
import {catApi} from "../app/api/catApi";

export const rootReducer = combineReducers({
    usersSlice: cardSlice,
    [api.reducerPath]: api.reducer,
})
export const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(api.middleware, catApi.middleware),
})

export type RootState = ReturnType<typeof store.getState>
type AppDispatch = typeof store.dispatch

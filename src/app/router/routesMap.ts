export const ROUTES_MAP = {
    HOME: {
        PATH: "/",
        FULL_PATH: "/",
    },
    USER_BY_ID: {
        PATH: "/:id",
        FULL_PATH: "/:id",
    }
}

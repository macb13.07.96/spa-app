import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";

export const baseCatImageURL = "https://cataas.com";

export const api = createApi({
    tagTypes: ['ImageList'],
    reducerPath: "api",
    baseQuery: fetchBaseQuery({
        baseUrl: baseCatImageURL,
    }),
    endpoints: () => ({}),
});


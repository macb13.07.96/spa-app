import {api} from "../index";

interface IGetImageListQueryProps {
    page: number,
}

interface IGetImageListResponse {
    _id: string,
    mimetype: string,
    size: number,
    tags: string[]
}

export const catApi = api.injectEndpoints({
    endpoints: ({query}) => ({
        getImageList: query<IGetImageListResponse[], IGetImageListQueryProps>({
            query: () => `/api/cats?skip=0&limit=100`,
            serializeQueryArgs: (data) => {
                return data.endpointName
            },
            providesTags: ['ImageList']
        })
    })
})

export const {useGetImageListQuery} = catApi
